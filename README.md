## Challenge

#### 1 - déployer la base de données GSB fournies   
#### 2 - dans une documentation technique comportant votre nom, prénom, date, réaliser ou récupérer automatiquement le schéma physique de la base de données   
#### 3 - dans le document technique ajouter la requete SQL et le resutlat de :     
3.1 - Afficher  les praticiens classés par ordre alphabétique.   
3.2 - Diminuer les prixHT des échantillons supérieur à 2 € de 0.1%   
3.3 - Afficher l’ensemble de la description des médicaments qui n’ont pas été proposé aux praticiens   
#### 4 - permettre l'affiche grâce à PHP ou JS de 3.1   
#### 5 - Ajouter la table région avec les données fournies dans region.data , les visites sont effectuées dans des régions. Une région peut avoir plusieurs visites.  